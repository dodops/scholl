require 'rails_helper'

RSpec.feature "Create Course", type: :feature do
  context 'when a user fills the form with valid data' do
    it 'allows the creation' do
      visit new_course_path
      fill_in 'course[name]', with: 'Matematica test'
      fill_in 'course[description]', with: 'Umas treta ai test'
      select 'Ativo', from: 'course[status]'

      click_button I18n.t('helpers.submit.course.create')

      expect(page).to have_content('Curso foi criado com sucesso.')
    end
  end

  context 'when a user fills the form with incorrect data' do
    it 'dosent allow the creation' do
      visit new_course_path

      click_button I18n.t('helpers.submit.course.create')

      expect(page).to_not have_content('Curso foi criado com sucesso.')
    end
  end
end
