require 'rails_helper'

RSpec.feature "Create Student", type: :feature do
  context 'when a user fills the form with valid data' do
    it 'allows the creation' do
      visit new_student_path
      fill_in 'student[name]', with: 'User test'
      select 'Ativo', from: 'student[status]'

      click_button I18n.t('helpers.submit.student.create')

      expect(page).to have_content('Estudante foi criado com sucesso.')
    end
  end

  context 'when a user fills the form with incorrect data' do
    it 'dosent allow the creation' do
      visit new_student_path

      click_button I18n.t('helpers.submit.student.create')

      expect(page).to_not have_content('Estudante foi criado com sucesso.')
    end
  end
end
