require 'rails_helper'

RSpec.describe StudentsController, type: :controller do
  let(:valid_attributes) { attributes_for(:student) }
  let(:invalid_attributes) { attributes_for(:student, name: nil) }
  let(:student) { create(:student) }

  describe "GET #index" do
    it "assigns all students as @students" do
      get :index, {}
      expect(assigns(:students)).to eq([student])
    end
  end

  describe "GET #show" do
    it "assigns the requested student as @student" do
      get :show, {:id => student.to_param}
      expect(assigns(:student)).to eq(student)
    end
  end

  describe "GET #new" do
    it "assigns a new student as @student" do
      get :new, {}
      expect(assigns(:student)).to be_a_new(Student)
    end
  end

  describe "GET #edit" do
    it "assigns the requested student as @student" do
      get :edit, {:id => student.to_param}
      expect(assigns(:student)).to eq(student)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Student" do
        expect {
          post :create, {:student => valid_attributes}
        }.to change(Student, :count).by(1)
      end

      it "assigns a newly created student as @student" do
        post :create, {:student => valid_attributes}
        expect(assigns(:student)).to be_a(Student)
        expect(assigns(:student)).to be_persisted
      end

      it "redirects to the created student" do
        post :create, {:student => valid_attributes}
        expect(response).to redirect_to(Student.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved student as @student" do
        post :create, {:student => invalid_attributes}
        expect(assigns(:student)).to be_a_new(Student)
      end

      it "re-renders the 'new' template" do
        post :create, {:student => invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { attributes_for(:student, name: 'New') }

      it "updates the requested student" do
        put :update, {:id => student.to_param, :student => new_attributes}
        student.reload
        expect(student.name).to eq(new_attributes[:name])
      end

      it "assigns the requested student as @student" do
        put :update, {:id => student.to_param, :student => valid_attributes}
        expect(assigns(:student)).to eq(student)
      end

      it "redirects to the student" do
        put :update, {:id => student.to_param, :student => valid_attributes}
        expect(response).to redirect_to(student)
      end
    end

    context "with invalid params" do
      it "assigns the student as @student" do
        put :update, {:id => student.to_param, :student => invalid_attributes}
        expect(assigns(:student)).to eq(student)
      end

      it "re-renders the 'edit' template" do
        put :update, {:id => student.to_param, :student => invalid_attributes}
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:student) { create(:student) }
    it "destroys the requested student" do
      expect {
        delete :destroy, {:id => student.to_param}
      }.to change(Student, :count).by(-1)
    end

    it "redirects to the students list" do
      delete :destroy, {:id => student.to_param}
      expect(response).to redirect_to(students_url)
    end
  end

end
