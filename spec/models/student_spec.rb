require 'rails_helper'

RSpec.describe Student, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:status) }

  describe "#genereate_register" do
    let(:student) { build(:student) }

    it 'generates a register number to students' do
      expect(student.register_number).to be_nil
      student.save
      expect(student.register_number).to_not be_nil
    end
  end
end
