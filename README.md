# Scholl

URL: https://secure-tor-46767.herokuapp.com/

[![Circle CI](https://circleci.com/gh/dodops/scholl.svg?style=shield&circle-token=cb576944d38752ea18160c52453e31324c99f935)](https://circleci.com/gh/dodops/scholl/)

Project made for a job interview

## Installation

### Main Dependencies

- Ruby;
- PostgreSQL;

### 1 - Install dependecy

Clone the repo on your machine, after that, just install the gems

```console
bundle install
```

### 2 - Database setup

```console
rake db:setup
```

## Exec

To create a local server with this app, you must run in your console:

```console
rails s
```
