Rails.application.routes.draw do
  resources :courses
  resources :students
  resources :classrooms, only: [:create, :new]

  root 'courses#index'
end
