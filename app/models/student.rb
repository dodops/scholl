class Student < ActiveRecord::Base
  has_many :classrooms
  has_many :courses, :through => :classrooms

  validates :name, :status, presence: true
  before_create :generate_register

  has_enumeration_for :status, create_helpers: true, create_scopes: true
  private

    def generate_register
      self.register_number = SecureRandom.hex(6)
      generate_register if Student.exists?(register_number: self.register_number)
    end
end
