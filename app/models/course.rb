class Course < ActiveRecord::Base
  has_many :classrooms
  has_many :students, :through => :classrooms

  validates :name, :description, :status, presence: true

  has_enumeration_for :status, create_helpers: true, create_scopes: true
end
