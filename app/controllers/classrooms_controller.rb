class ClassroomsController < ApplicationController
  respond_to :html

  def create
    @classroom = Classroom.find_or_initialize_by(classroom_params)
    if @classroom.save
      @student = @classroom.student
      flash[:notice] = t('flash.classrooms.create')
      redirect_to @student
    else
      render :new
    end
  end

  def new
    @classroom = Classroom.new
  end

  private
    def classroom_params
      params.require(:classroom).permit(:student_id, :course_id)
    end
end
